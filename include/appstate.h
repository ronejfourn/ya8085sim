#pragma once
#include <cstddef>
#include <stdint.h>
#include <vector>
#include <string_view>

struct line_t
{
    std::string_view data;
    int address;
    uint8_t nbytes = 0;
};

struct lstring_t
{
    char *data;
    size_t length;
};

struct appstate_t
{
    lstring_t code;
    uint16_t loadat = 0x4200;
    bool execute = false;
    bool single_step = false;
    bool assembled = false;
    std::vector<line_t> lines;
};
