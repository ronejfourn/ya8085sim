#pragma once
#include <cstdint>
enum {
    REG_A, REG_F, REG_B, REG_C,
    REG_D, REG_E, REG_H, REG_L,
    REG_COUNT,
};

enum {
    FLAG_NONE = 0,
    FLAG_S    = 1 << 7,
    FLAG_Z    = 1 << 6,
    FLAG_AC   = 1 << 4,
    FLAG_P    = 1 << 3,
    FLAG_CY   = 1,
};

struct cpu_t
{
    uint8_t registers[REG_COUNT];
    uint8_t ports[0x100];
    uint8_t memory[0x10000];
    uint16_t WZ = 0x0000;
    uint16_t PC = 0x0000;
    uint16_t SP = 0xFFFF;
};

bool execute(cpu_t &CPU);

#define UPPER_BYTE_B CPU.registers[REG_B]
#define UPPER_BYTE_D CPU.registers[REG_D]
#define UPPER_BYTE_H CPU.registers[REG_H]
#define UPPER_BYTE_SP *((uint8_t*)&CPU.SP)
#define UPPER_BYTE_PSW CPU.registers[REG_A]

#define LOWER_BYTE_B CPU.registers[REG_C]
#define LOWER_BYTE_D CPU.registers[REG_E]
#define LOWER_BYTE_H CPU.registers[REG_L]
#define LOWER_BYTE_SP *((uint8_t*)&CPU.SP + 1)
#define LOWER_BYTE_PSW CPU.registers[REG_F]
#define REG_PAIR(x) ((uint16_t)UPPER_BYTE_ ##x << 8 | (uint16_t)LOWER_BYTE_ ##x)

#define _REG__A CPU.registers[REG_A]
#define _REG__F CPU.registers[REG_F]
#define _REG__B CPU.registers[REG_B]
#define _REG__C CPU.registers[REG_C]
#define _REG__D CPU.registers[REG_D]
#define _REG__E CPU.registers[REG_E]
#define _REG__H CPU.registers[REG_H]
#define _REG__L CPU.registers[REG_L]
#define _REG__M CPU.memory[REG_PAIR(H)]
#define REGISTER(x) _REG__ ##x


#define SET(f, b) ((f) |= (b))
#define RESET(f, b) ((f) &= ~(b))
#define TOGGLE(f, b) ((f) ^= (b))
