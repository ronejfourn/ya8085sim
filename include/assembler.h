#pragma once
#include "cpu.h"
#include <cstdint>
#include <cstddef>
#include <string>
#include "appstate.h"

bool assemble();
void assembler_init(cpu_t *CPU, appstate_t *state);
std::pair<int, std::string> assembler_error();
