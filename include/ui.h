#pragma once
#include "cpu.h"
#include "appstate.h"
#include <GLFW/glfw3.h>

bool ui_init(GLFWwindow *window, cpu_t *CPU, appstate_t *state);
void ui_end();
void ui_render();
