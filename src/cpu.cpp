#include "cpu.h"
#include "instructions.h"

static uint8_t ALU(cpu_t &CPU, uint8_t op1, uint8_t op2, char op, uint8_t cc) {
    uint8_t res;
    switch (op) {
        case '-': op2 = ~op2 + 1;
        case '+': res = op1 + op2; break;
        case '&': res = op1 & op2; break;
        case '|': res = op1 | op2; break;
        case '^': res = op1 ^ op2; break;
    }

    res == 0 ?
        SET(REGISTER(F), FLAG_Z):
        RESET(REGISTER(F), FLAG_Z);

    (res & 0x80) == 0 ?
        RESET(REGISTER(F), FLAG_S):
        SET(REGISTER(F), FLAG_S);

    if (cc) {
        if (op == '-')
            (op1 + op2 > 0xff || op2 == 0) ?
                RESET(REGISTER(F), FLAG_CY):
                SET(REGISTER(F), FLAG_CY);
        else if (op == '+')
            (op1 + op2 > 0xff) ?
                SET(REGISTER(F), FLAG_CY):
                RESET(REGISTER(F), FLAG_CY);
    }

    uint8_t cnt = 0;
    for (uint8_t cpy = res; cpy != 0; cpy >>= 1)
        cnt += cpy & 1;
    (cnt & 1) ?
        RESET(REGISTER(F), FLAG_P):
        SET(REGISTER(F), FLAG_P);

    ((op1 & 0xf) + (op2 & 0xf) > 0xf) ?
        SET(REGISTER(F), FLAG_AC):
        RESET(REGISTER(F), FLAG_AC);

    return res;
}

bool execute(cpu_t &CPU)
{
    auto& SP = CPU.SP;
    auto& PC = CPU.PC;
    auto& WZ = CPU.WZ;
    auto& memory = CPU.memory;
    auto& ports = CPU.ports;

    switch (CPU.memory[CPU.PC]) {
#define PUSH(rp) \
    case PUSH_ ##rp: \
        memory[--SP] = UPPER_BYTE_ ##rp; \
        memory[--SP] = LOWER_BYTE_ ##rp; \
        PC++; \
        break

        PUSH(B);
        PUSH(D);
        PUSH(H);
        PUSH(PSW);
#undef PUSH

#define POP(rp) \
    case POP_ ##rp: \
        LOWER_BYTE_ ##rp = memory[SP++]; \
        UPPER_BYTE_ ##rp = memory[SP++]; \
        PC++; \
        break

        POP(B);
        POP(D);
        POP(H);
        POP(PSW);
#undef POP

    case XTHL:
        WZ = REGISTER(L);
        REGISTER(L) = memory[SP];
        memory[SP] = WZ;
        WZ = REGISTER(H);
        REGISTER(H) = memory[SP + 1];
        memory[SP + 1] = WZ;
        ++PC; break;

    case SPHL:
        SP = REG_PAIR(H);
        ++PC; break;

#define MVI(x) \
case MVI_ ##x : \
    REGISTER(x) = memory[++PC]; \
    ++PC; \
    break

        MVI(A);
        MVI(B);
        MVI(C);
        MVI(D);
        MVI(E);
        MVI(H);
        MVI(L);
        MVI(M);
#undef MVI

#define MOV(x, y) \
case MOV_ ##x## _ ##y : \
    REGISTER(x) = REGISTER(y); \
    ++PC; \
    break

        MOV(A, A); MOV(B, A); MOV(C, A); MOV(D, A);
        MOV(A, B); MOV(B, B); MOV(C, B); MOV(D, B);
        MOV(A, C); MOV(B, C); MOV(C, C); MOV(D, C);
        MOV(A, D); MOV(B, D); MOV(C, D); MOV(D, D);
        MOV(A, E); MOV(B, E); MOV(C, E); MOV(D, E);
        MOV(A, H); MOV(B, H); MOV(C, H); MOV(D, H);
        MOV(A, L); MOV(B, L); MOV(C, L); MOV(D, L);
        MOV(A, M); MOV(B, M); MOV(C, M); MOV(D, M);

        MOV(E, A); MOV(H, A); MOV(L, A); MOV(M, A);
        MOV(E, B); MOV(H, B); MOV(L, B); MOV(M, B);
        MOV(E, C); MOV(H, C); MOV(L, C); MOV(M, C);
        MOV(E, D); MOV(H, D); MOV(L, D); MOV(M, D);
        MOV(E, E); MOV(H, E); MOV(L, E); MOV(M, E);
        MOV(E, H); MOV(H, H); MOV(L, H); MOV(M, H);
        MOV(E, L); MOV(H, L); MOV(L, L); MOV(M, L);
        MOV(E, M); MOV(H, M); MOV(L, M);
#undef MOV

    case LDAX_B:
        REGISTER(A) = memory[REG_PAIR(B)];
        ++PC; break;

    case LDAX_D:
        REGISTER(A) = memory[REG_PAIR(D)];
        ++PC; break;

    case STAX_B:
        memory[REG_PAIR(B)] = REGISTER(A);
        ++PC; break;

    case STAX_D:
        memory[REG_PAIR(D)] = REGISTER(A);
        ++PC; break;

    case LHLD:
        WZ = (memory[PC + 1] & 0xFF) | ((memory[PC + 2] & 0xFF) << 8);
        REGISTER(L) = memory[WZ];
        REGISTER(H) = memory[WZ + 1];
        PC += 3; break;

    case SHLD:
        WZ = (memory[PC + 1] & 0xFF) | ((memory[PC + 2] & 0xFF) << 8);
        memory[WZ] = REGISTER(L);
        memory[WZ + 1] = REGISTER(H);
        PC += 3; break;

    case LDA:
        WZ = (memory[PC + 1] & 0xFF) | ((memory[PC + 2] & 0xFF) << 8);
        REGISTER(A) = memory[WZ];
        PC += 3; break;

    case STA:
        WZ = (memory[PC + 1] & 0xFF) | ((memory[PC + 2] & 0xFF) << 8);
        memory[WZ] = REGISTER(A);
        PC += 3; break;

#define LXI(rp) \
    case LXI_ ##rp: \
        LOWER_BYTE_ ##rp = memory[++PC]; \
        UPPER_BYTE_ ##rp = memory[++PC]; \
        ++PC; \
        break

    LXI(B);
    LXI(D);
    LXI(H);
    LXI(SP);
#undef LXI

    case XCHG:
        WZ = REGISTER(L);
        REGISTER(L) = REGISTER(E);
        REGISTER(E) = WZ;
        WZ = REGISTER(H);
        REGISTER(H) = REGISTER(D);
        REGISTER(D) = WZ;
        ++PC; break;

#define ADD(x) \
    case ADD_ ##x: { \
        REGISTER(A) = ALU(CPU, REGISTER(A), REGISTER(x), '+', 1); \
        PC ++; \
    } break

        ADD(A);
        ADD(B);
        ADD(C);
        ADD(D);
        ADD(E);
        ADD(H);
        ADD(L);
        ADD(M);
#undef ADD

    case ADI: {
        REGISTER(A) = ALU(CPU, REGISTER(A), memory[++PC], '+', 1);
        PC ++;
    } break;

#define ADC(x) \
    case ADC_ ##x: { \
        REGISTER(A) = ALU(CPU, REGISTER(A), REGISTER(x) + ((REGISTER(F) & FLAG_CY) != 0), '+', 1); \
        PC ++; \
    } break

        ADC(A);
        ADC(B);
        ADC(C);
        ADC(D);
        ADC(E);
        ADC(H);
        ADC(L);
        ADC(M);
#undef ADC

    case ACI: {
        REGISTER(A) = ALU(CPU, REGISTER(A), memory[++PC] + ((REGISTER(F) & FLAG_CY) != 0), '+', 1);
        PC ++;
    } break;

#define SUB(x) \
    case SUB_ ##x: { \
        REGISTER(A) = ALU(CPU, REGISTER(A), REGISTER(x), '-', 1); \
        PC ++; \
    } break

        SUB(A);
        SUB(B);
        SUB(C);
        SUB(D);
        SUB(E);
        SUB(H);
        SUB(L);
        SUB(M);
#undef SUB

    case SUI: {
        REGISTER(A) = ALU(CPU, REGISTER(A), memory[++PC], '-', 1);
        PC ++;
    } break;

#define SBB(x) \
    case SBB_ ##x: { \
        REGISTER(A) = ALU(CPU, REGISTER(A), REGISTER(x) + ((REGISTER(F) & FLAG_CY) != 0), '-', 1); \
        PC ++; \
    } break

        SBB(A);
        SBB(B);
        SBB(C);
        SBB(D);
        SBB(E);
        SBB(H);
        SBB(L);
        SBB(M);
#undef SBB

    case SBI: {
        REGISTER(A) = ALU(CPU, REGISTER(A), memory[++PC] + ((REGISTER(F) & FLAG_CY) != 0), '-', 1);
        PC ++;
    } break;

#define INR(x) \
    case INR_ ##x: { \
        REGISTER(x) = ALU(CPU, REGISTER(x), 1, '+', 0); \
        PC++; \
    } break

        INR(A);
        INR(B);
        INR(C);
        INR(D);
        INR(E);
        INR(H);
        INR(L);
        INR(M);
#undef INR

#define DCR(x) \
    case DCR_ ##x: { \
        REGISTER(x) = ALU(CPU, REGISTER(x), 1, '-', 0); \
        PC++; \
    } break

        DCR(A);
        DCR(B);
        DCR(C);
        DCR(D);
        DCR(E);
        DCR(H);
        DCR(L);
        DCR(M);
#undef DCR

#define INX(rp) \
    case INX_ ##rp: \
        UPPER_BYTE_ ##rp += (++LOWER_BYTE_ ##rp == 0x00); \
        PC++; \
        break

        INX(B);
        INX(D);
        INX(H);
        INX(SP);
#undef INX

#define DCX(rp) \
    case DCX_ ##rp: \
        UPPER_BYTE_ ##rp -= (--LOWER_BYTE_ ##rp == 0xff); \
        PC++; \
        break

        DCX(B);
        DCX(D);
        DCX(H);
        DCX(SP);
#undef DCX

#define DAD(rp) \
    case DAD_ ##rp: { \
        char ic = (LOWER_BYTE_H + LOWER_BYTE_ ##rp) > 0xFF;\
        (REG_PAIR(rp) + REG_PAIR(H) > 0xFFFF) ? \
            SET(REGISTER(F), FLAG_CY): \
            RESET(REGISTER(F), FLAG_CY); \
        LOWER_BYTE_H += LOWER_BYTE_ ##rp; \
        UPPER_BYTE_H += UPPER_BYTE_ ##rp; \
        UPPER_BYTE_H += ic; \
        PC ++; \
    } break

        DAD(B);
        DAD(D);
        DAD(H);
        DAD(SP);
#undef DAD

#define CMP(x) \
    case CMP_ ##x: { \
        ALU(CPU, REGISTER(A), REGISTER(x), '-', 1); \
        PC++; \
    } break

        CMP(A);
        CMP(B);
        CMP(C);
        CMP(D);
        CMP(E);
        CMP(H);
        CMP(L);
        CMP(M);
#undef CMP

    case DAA: {
        uint8_t un = (REGISTER(A) >> 4) & 0xF;
        uint8_t ln = (REGISTER(A) >> 0) & 0xF;
        if (ln > 0x9 || REGISTER(F) & FLAG_AC) {
            ln += 0x06;
            un += (ln > 0x0f);
            un %= 0x10;
            if (un == 0) SET(REGISTER(F), FLAG_CY);
            ln %= 0x10;
        }
        if (un > 0x9 || REGISTER(F) & FLAG_CY) {
            un += 0x06;
            if (un > 0x0f)
                SET(REGISTER(F), FLAG_CY);
            un %= 0x10;
        }
        REGISTER(A) = ln | un << 4;
    } ++PC; break;

    case RAL: {
        uint8_t c = (REGISTER(F) & FLAG_CY) != 0;
        (REGISTER(A) & 0x80) ?
            SET(REGISTER(F), FLAG_CY):
            RESET(REGISTER(F), FLAG_CY);
        REGISTER(A) <<= 1;
        REGISTER(A) |= c;
    } PC++; break;

    case RAR: {
        uint8_t c = (REGISTER(F) & FLAG_CY) != 0;
        (REGISTER(A) & 0x01) ?
            SET(REGISTER(F), FLAG_CY):
            RESET(REGISTER(F), FLAG_CY);
        REGISTER(A) >>= 1;
        REGISTER(A) |= (c << 7);
    } PC++; break;

    case RLC: {
        uint8_t c = (REGISTER(A) & 0x80) != 0;
        (c) ?
            SET(REGISTER(F), FLAG_CY):
            RESET(REGISTER(F), FLAG_CY);
        REGISTER(A) <<= 1;
        REGISTER(A) |= c;
    } PC++; break;

    case RRC: {
        uint8_t c = (REGISTER(A) & 0x01) != 0;
        (c) ?
            SET(REGISTER(F), FLAG_CY):
            RESET(REGISTER(F), FLAG_CY);
        REGISTER(A) >>= 1;
        REGISTER(A) |= (c << 7);
    } PC++; break;

#define ANA(x) \
    case ANA_ ##x: \
        REGISTER(A) = ALU(CPU, REGISTER(A), REGISTER(x), '&', 0); \
        SET(REGISTER(F), FLAG_AC); \
        RESET(REGISTER(F), FLAG_CY); \
        PC ++; break

        ANA(A);
        ANA(B);
        ANA(C);
        ANA(D);
        ANA(E);
        ANA(H);
        ANA(L);
        ANA(M);
#undef ANA

    case ANI:
        REGISTER(A) = ALU(CPU, REGISTER(A), memory[++PC], '&', 0);
        SET(REGISTER(F), FLAG_AC);
        RESET(REGISTER(F), FLAG_CY);
        PC ++; break;

#define XRA(x) \
    case XRA_ ##x: \
        REGISTER(A) = ALU(CPU, REGISTER(A), REGISTER(x), '^', 0); \
        RESET(REGISTER(F), FLAG_AC); \
        RESET(REGISTER(F), FLAG_CY); \
        PC ++; break

        XRA(A);
        XRA(B);
        XRA(C);
        XRA(D);
        XRA(E);
        XRA(H);
        XRA(L);
        XRA(M);
#undef XRA

    case XRI:
        REGISTER(A) = ALU(CPU, REGISTER(A), memory[++PC], '^', 0);
        RESET(REGISTER(F), FLAG_AC);
        RESET(REGISTER(F), FLAG_CY);
        PC ++; break;

#define ORA(x) \
    case ORA_ ##x: \
        REGISTER(A) = ALU(CPU, REGISTER(A), REGISTER(x), '|', 0); \
        RESET(REGISTER(F), FLAG_AC); \
        RESET(REGISTER(F), FLAG_CY); \
        PC ++; break

        ORA(A);
        ORA(B);
        ORA(C);
        ORA(D);
        ORA(E);
        ORA(H);
        ORA(L);
        ORA(M);
#undef ORA

    case ORI:
        REGISTER(A) = ALU(CPU, REGISTER(A), memory[++PC], '|', 0);
        RESET(REGISTER(F), FLAG_AC);
        RESET(REGISTER(F), FLAG_CY);
        PC ++; break;

    case CMA:
        REGISTER(A) = ~REGISTER(A);
        ++PC; break;

    case STC:
        SET(REGISTER(F), FLAG_CY);
        ++PC; break;

    case CMC:
        TOGGLE(REGISTER(F), FLAG_CY);
        ++PC; break;

    case CPI: {
        ALU(CPU, REGISTER(A), memory[++PC], '-', 1);
        PC++;
    } break;

    case JMP:
        PC = (uint16_t)memory[PC + 2] << 8 | memory[PC + 1];
        break;

#define JMP_ON_TRUE(flag) \
    PC = (REGISTER(F) & FLAG_ ##flag) ? \
        (uint16_t)memory[PC + 2] << 8 | memory[PC + 1] : PC + 3; \
    break

    case JZ : JMP_ON_TRUE(Z);
    case JPE: JMP_ON_TRUE(P);
    case JC : JMP_ON_TRUE(CY);
    case JM : JMP_ON_TRUE(S);
#undef JMP_ON_TRUE

#define JMP_ON_FALSE(flag) \
    PC = (REGISTER(F) & FLAG_ ##flag) ? \
        PC + 3 : (uint16_t)memory[PC + 2] << 8 | memory[PC + 1]; \
    break

    case JNZ: JMP_ON_FALSE(Z);
    case JPO: JMP_ON_FALSE(P);
    case JNC: JMP_ON_FALSE(CY);
    case JP : JMP_ON_FALSE(S);
#undef JMP_ON_FALSE

    case CALL:
        memory[--SP] = PC >> 8;
        memory[--SP] = PC & 0xff;
        PC = (uint16_t)memory[PC + 2] << 8 | memory[PC + 1];
        break;

#define CALL_ON_TRUE(flag) \
    if (REGISTER(F) & FLAG_ ##flag) { \
        memory[--SP] = PC >> 8; \
        memory[--SP] = PC & 0xff; \
        PC = (uint16_t)memory[PC + 2] << 8 | memory[PC + 1]; \
    } else { \
        PC += 3;\
    } break

    case CZ : CALL_ON_TRUE(Z);
    case CPE: CALL_ON_TRUE(P);
    case CC : CALL_ON_TRUE(CY);
    case CM : CALL_ON_TRUE(S);
#undef CALL_ON_TRUE

#define CALL_ON_FALSE(flag) \
    if (REGISTER(F) & FLAG_ ##flag) { \
        PC += 3;\
    } else { \
        memory[--SP] = PC >> 8; \
        memory[--SP] = PC & 0xff; \
        PC = (uint16_t)memory[PC + 2] << 8 | memory[PC + 1]; \
    } break

    case CNZ: CALL_ON_FALSE(Z);
    case CPO: CALL_ON_FALSE(P);
    case CNC: CALL_ON_FALSE(CY);
    case CP : CALL_ON_FALSE(S);
#undef CALL_ON_FALSE

    case RET:
        PC = (uint16_t)memory[SP + 1] << 8 | memory[SP];
        SP += 2;
        break;

#define RET_ON_TRUE(flag) \
    if (REGISTER(F) & FLAG_ ##flag) { \
        PC = (uint16_t)memory[SP + 1] << 8 | memory[SP]; \
        SP += 2; \
    } else { \
        PC += 3; \
    } break

    case RZ : RET_ON_TRUE(Z);
    case RPE: RET_ON_TRUE(P);
    case RC : RET_ON_TRUE(CY);
    case RM : RET_ON_TRUE(S);
#undef RET_ON_TRUE

#define RET_ON_FALSE(flag) \
    if (REGISTER(F) & FLAG_ ##flag) { \
        PC += 3; \
    } else { \
        PC = (uint16_t)memory[SP + 1] << 8 | memory[SP]; \
        SP += 2; \
    } break

    case RNZ: RET_ON_FALSE(Z);
    case RPO: RET_ON_FALSE(P);
    case RNC: RET_ON_FALSE(CY);
    case RP : RET_ON_FALSE(S);
#undef RET_ON_FALSE

    case PCHL:
        PC = REG_PAIR(H);
        break;

    case RST_0:
    case RST_1:
    case RST_2:
    case RST_3:
    case RST_4:
    case RST_5:
    case RST_6:
    case RST_7:
    case HLT:
        ++PC;
        return false;
        break;

    case EI:
    case DI:
    case NOP:
        ++PC; break;

    case IN:
        REGISTER(A) = ports[memory[++PC]];
        ++PC; break;

    case OUT:
        ports[memory[++PC]] = REGISTER(A);
        ++PC; break;

    default: break;
    }
    return true;
}
