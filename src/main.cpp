#include <cstddef>
#include <cstdio>
#include <cstring>
#include "ui.h"
#include "cpu.h"
#include "assembler.h"
#include "appstate.h"
#include <cstdlib>

static void glfw_error_callback(int error, const char* description)
{
    fprintf(stderr, "Glfw Error %d: %s\n", error, description);
}

static void glfw_framebuffer_size_callback(GLFWwindow *window, int w, int h)
{
    glViewport(0, 0, w, h);
}

char *read_entire_file(const char *fname, size_t *length)
{
    size_t size = 0;
    FILE *fptr  = nullptr;
    char *bufr  = nullptr;
    *length = 0;
    fptr = fopen(fname, "rb");
    if (!fptr) goto fail;
    fseek(fptr, 0, SEEK_END);
    size = ftell(fptr);
    fseek(fptr, 0, SEEK_SET);
    bufr = new char[size + 1];
    if (!bufr) goto fail;
    fread(bufr, 1, size, fptr);
    bufr[size] = 0;
    fclose(fptr);
    *length = size;
    return bufr;
fail:
    if (fptr) fclose(fptr);
    return 0;
}

int main(int argc, char **argv)
{
    size_t length = 0;
    char *data = nullptr;

    if (argc == 2)
    {
        data = read_entire_file(argv[1], &length);
    }

    if (!data)
    {
        data   = (char *)calloc(512, 1);
        if (!data)
        {
            fprintf(stderr, "OUT OF MEMORY\n");
            return 1;
        }
        length = 512;
    }

    glfwSetErrorCallback(glfw_error_callback);
    if (!glfwInit())
        return 1;

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#if defined(__APPLE__)
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

    auto window = glfwCreateWindow(1280, 720, "Yet Another 8085 Simulator", NULL, NULL);
    if (window == NULL)
        return 1;
    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, glfw_framebuffer_size_callback);
    glfwSwapInterval(1); // Enable vsync
    struct {float x, y, z, w;} clear_color = {0.45f, 0.55f, 0.60f, 1.00f};

    cpu_t CPU;
    memset(CPU.registers, 0, sizeof(CPU.registers));
    memset(CPU.memory, 0, sizeof(CPU.memory));
    memset(CPU.ports, 0, sizeof(CPU.ports));

    appstate_t state;
    state.loadat   = 0x4200;
    state.execute  = false;
    state.single_step = false;
    state.code.data   = data;
    state.code.length = length;

    ui_init(window, &CPU, &state);
    assembler_init(&CPU, &state);

    while (!glfwWindowShouldClose(window))
    {
        glfwPollEvents();
        glClearColor(clear_color.x * clear_color.w, clear_color.y * clear_color.w, clear_color.z * clear_color.w, clear_color.w);
        glClear(GL_COLOR_BUFFER_BIT);

        if (state.execute) state.execute = execute(CPU);
        state.execute = !state.single_step && state.execute;

        ui_render();
        glfwSwapBuffers(window);
    }

    glfwDestroyWindow(window);
    glfwTerminate();
}
