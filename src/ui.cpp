#include <cstdint>
#include <cstdio>
#include <stdlib.h>

#include "imgui.h"
#include "backends/imgui_impl_glfw.h"
#include "backends/imgui_impl_opengl3.h"
#include "ui.h"
#include "assembler.h"

struct
{
    cpu_t *CPU;
    appstate_t *state;
    int err_line;
    bool changed = false;
} UI;

static void render_code();
static void render_registers();
static void render_listing();
static void render_stack();
static void render_ports();
static void render_memory();
static void render_assembler();

bool ui_init(GLFWwindow *window, cpu_t *CPU, appstate_t *state)
{
    UI.CPU = CPU;
    UI.state = state;

    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;       // Enable Keyboard Controls
    io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;           // Enable Docking

    ImFontConfig cfg = ImFontConfig();
    cfg.SizePixels = 16.0f;
    cfg.OversampleH = cfg.OversampleV = 1;
    cfg.PixelSnapH = true;
    io.Fonts->AddFontDefault(&cfg);

    // Setup Dear ImGui style
    ImGui::StyleColorsDark();

    // When viewports are enabled we tweak WindowRounding/WindowBg so platform windows can look identical to regular ones.
    ImGuiStyle& style = ImGui::GetStyle();
    if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
    {
        style.WindowRounding = 0.0f;
        style.Colors[ImGuiCol_WindowBg].w = 1.0f;
    }

    // Setup Platform/Renderer backends
    ImGui_ImplGlfw_InitForOpenGL(window, true);
    const char* glsl_version = "#version 130";
    ImGui_ImplOpenGL3_Init(glsl_version);
    return true;
}

void ui_end()
{
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();
}

void ui_render()
{
    // Start the Dear ImGui frame
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();
    ImGui::DockSpaceOverViewport(ImGui::GetMainViewport());

    render_code();
    render_registers();
    render_listing();
    render_stack();
    render_ports();
    render_memory();
    render_assembler();

    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}

static void render_code()
{
    ImGui::Begin("Code");
    ImGui::PushItemWidth(100);
    ImGui::Text("Load at (Hex)"); ImGui::SameLine();
    ImGui::InputScalar("##loadathex", ImGuiDataType_U16, &UI.state->loadat, NULL, NULL, "%x"); ImGui::SameLine();
    ImGui::Text("Load at (Dec)"); ImGui::SameLine();
    ImGui::InputScalar("##loadatdec", ImGuiDataType_U16, &UI.state->loadat, NULL, NULL, "%d"); ImGui::SameLine();
    ImGui::PopItemWidth();

    ImGui::PushItemWidth(-100);
    if (ImGui::Button("Assemble"))
    {
        UI.CPU->PC = UI.state->loadat;
        UI.state->assembled = assemble();
        UI.changed = false;
        UI.err_line = -1;
    }

    ImGui::SameLine();
    if (ImGui::Button("Run"))
    {
        UI.CPU->PC = UI.state->loadat;
        UI.state->assembled = assemble();
        UI.state->execute = UI.state->assembled;
        UI.changed = false;
    }

    ImGui::SameLine();
    if (ImGui::Button("Stop"))
    {
        UI.CPU->PC = UI.state->loadat;
        UI.state->execute = false;
    }

    ImGui::Checkbox("Single Step", &UI.state->single_step);
    ImGui::SameLine();
    if (ImGui::Button("Step"))
    {
        UI.state->single_step = true;
        if (!UI.state->assembled)
            UI.state->assembled = assemble();
        UI.state->execute = UI.state->assembled;
        UI.changed = false;
    }
    ImGui::PopItemWidth();

    static const auto resize_cb = [](ImGuiInputTextCallbackData* data) -> int
    {
        if (data->EventFlag == ImGuiInputTextFlags_CallbackResize)
        {
            lstring_t *my_str = (lstring_t *)data->UserData;
            IM_ASSERT(my_str->data == data->Buf);
            my_str->data = (char *)realloc(my_str->data, data->BufSize + 30);
            my_str->length = data->BufSize + 30;
            data->Buf = my_str->data;
        }
        return 0;
    };

    ImGui::PushStyleColor(ImGuiCol_FrameBg, ImVec4(0.2f, 0.2f, 0.2f, 1.0f));
    bool changed = ImGui::InputTextMultiline("##code", UI.state->code.data, UI.state->code.length, ImGui::GetContentRegionAvail(),
            ImGuiInputTextFlags_CallbackResize, resize_cb, (void*)&UI.state->code);
    if (changed)
    {
        UI.state->assembled = false;
        UI.state->execute   = false;
        UI.changed = true;
    }
    ImGui::PopStyleColor();
    ImGui::End();
}

static void render_registers()
{
    cpu_t &CPU = *UI.CPU;
    ImGui::Begin("Registers and Flags");
    float h = ImGui::GetContentRegionAvail().y * 0.75f;
    ImGuiWindowFlags window_flags = ImGuiWindowFlags_None;
    ImGui::BeginChild("##rfL", ImVec2(ImGui::GetContentRegionAvail().x * 0.4f, h), true, window_flags);
    ImGui::Text("Flags"); ImGui::SameLine();
    if (ImGui::Button("Reset"))
        REGISTER(F) = 0;
    if (ImGui::BeginTable("Flags", 2))
    {
        ImGui::TableSetupColumn("Name");
        ImGui::TableSetupColumn("Value");
        ImGui::TableHeadersRow();
        ImGui::TableNextRow();
        ImGui::TableSetColumnIndex(0);
        ImGui::Text("S");
        ImGui::TableSetColumnIndex(1);
        ImGui::Text("%d", (REGISTER(F) & FLAG_S) != 0);
        ImGui::TableNextRow();
        ImGui::TableSetColumnIndex(0);
        ImGui::Text("Z");
        ImGui::TableSetColumnIndex(1);
        ImGui::Text("%d", (REGISTER(F) & FLAG_Z) != 0);
        ImGui::TableNextRow();
        ImGui::TableSetColumnIndex(0);
        ImGui::Text("AC");
        ImGui::TableSetColumnIndex(1);
        ImGui::Text("%d", (REGISTER(F) & FLAG_AC) != 0);
        ImGui::TableNextRow();
        ImGui::TableSetColumnIndex(0);
        ImGui::Text("P");
        ImGui::TableSetColumnIndex(1);
        ImGui::Text("%d", (REGISTER(F) & FLAG_P) != 0);
        ImGui::TableNextRow();
        ImGui::TableSetColumnIndex(0);
        ImGui::Text("CY");
        ImGui::TableSetColumnIndex(1);
        ImGui::Text("%d", (REGISTER(F) & FLAG_CY) != 0);
        ImGui::EndTable();
    }
    ImGui::EndChild();
    ImGui::SameLine();
    ImGui::BeginChild("##rfR", ImVec2(0, h), true, window_flags);
    ImGui::Text("Registers"); ImGui::SameLine();
    if (ImGui::Button("Reset"))
        memset(UI.CPU->registers, 0, REG_COUNT);
    if (ImGui::BeginTable("Registers", 2))
    {
        ImGui::TableSetupColumn("Name");
        ImGui::TableSetupColumn("Value");
        ImGui::TableHeadersRow();
#define ROW(x) \
        ImGui::TableNextRow(); \
        ImGui::TableSetColumnIndex(0); \
        ImGui::Text(#x); \
        ImGui::TableSetColumnIndex(1); \
        ImGui::Text("%.2X", REGISTER(x))

        ROW(A);
        ROW(B);
        ROW(C);
        ROW(D);
        ROW(E);
        ROW(H);
        ROW(L);
#undef ROW
        ImGui::EndTable();
    }
    ImGui::EndChild();
    ImGui::BeginChild("##rfB", ImVec2(0, 0), true, window_flags);
    if (ImGui::BeginTable("Registers", 2))
    {
        ImGui::TableSetupColumn("Name");
        ImGui::TableSetupColumn("Value");
        ImGui::TableHeadersRow();
        ImGui::TableNextRow();
        ImGui::TableNextRow();
            ImGui::TableSetColumnIndex(0);
            ImGui::Text("SP");
            ImGui::TableSetColumnIndex(1);
            ImGui::Text("%.4X", UI.CPU->SP);
        ImGui::TableNextRow();
            ImGui::TableSetColumnIndex(0);
            ImGui::Text("PC");
            ImGui::TableSetColumnIndex(1);
            ImGui::Text("%.4X", UI.CPU->PC);
        ImGui::EndTable();
    }
    ImGui::EndChild();
    ImGui::End();
}

static void render_listing()
{
    const static ImGuiTableFlags flags = ImGuiTableFlags_RowBg | ImGuiTableFlags_Resizable;
    ImGui::Begin("Listing");
    if (ImGui::BeginTable("##memory", 3, flags))
    {
        ImGui::TableSetupColumn("Address (Hex)");
        ImGui::TableSetupColumn("Opcode");
        ImGui::TableSetupColumn("Assembly");
        ImGui::TableHeadersRow();

        int nlines = UI.state->lines.size();
        for (int i = 0; i < nlines && !UI.changed; i++)
        {
            ImGui::TableNextRow();
            line_t &line = UI.state->lines[i];
            if (line.address == UI.CPU->PC && UI.state->assembled)
            {
                ImU32 row_bg_color = ImGui::GetColorU32(ImVec4(0.3f, 0.7f, 0.3f, 0.65f));
                ImGui::TableSetBgColor(ImGuiTableBgTarget_RowBg0 + (i & 1), row_bg_color);
            }

            if (UI.err_line == i + 1)
            {
                ImU32 row_bg_color = ImGui::GetColorU32(ImVec4(0.7f, 0.3f, 0.3f, 0.65f));
                ImGui::TableSetBgColor(ImGuiTableBgTarget_RowBg0 + (i & 1), row_bg_color);
            }

            ImGui::TableSetColumnIndex(0);
            if(line.address != -1) ImGui::TextColored(ImVec4(0x98 / 255.0f, 0xBB / 255.0f, 0x6C / 255.0f, 1.0f), "%.4XH", line.address);
            ImGui::TableSetColumnIndex(1);
            uint16_t a = line.address;
            switch (line.nbytes)
            {
                case 1: ImGui::Text("%.2X", UI.CPU->memory[a]); break;
                case 2: ImGui::Text("%.2X %.2X", UI.CPU->memory[a], UI.CPU->memory[uint16_t(a + 1)]); break;
                case 3: ImGui::Text("%.2X %.2X %.2X", UI.CPU->memory[a], UI.CPU->memory[uint16_t(a + 1)], UI.CPU->memory[uint16_t(a + 2)]); break;
                default: break;
            }
            ImGui::TableSetColumnIndex(2);
            ImGui::Text("%.*s", uint32_t(line.data.length()), line.data.data());
        }
        ImGui::EndTable();
    }
    ImGui::End();
}

static void render_stack()
{
    const static ImGuiTableFlags flags = ImGuiTableFlags_RowBg | ImGuiTableFlags_Resizable;
    ImGui::Begin("Stack");
    ImGuiWindowFlags window_flags = ImGuiWindowFlags_None;
    ImGui::BeginChild("##stackT", ImVec2(0, 68), true, window_flags);

    ImGui::Text("Start (Hex)");
    ImGui::SameLine();
    static uint16_t stkstart = UI.CPU->SP;
    ImGui::InputScalar("##stkhex", ImGuiDataType_U16, &stkstart, NULL, NULL, "%x");
    ImGui::Text("Start (Dec)");
    ImGui::SameLine();
    ImGui::InputScalar("##stkdec", ImGuiDataType_U16, &stkstart, NULL, NULL, "%u");

    ImGui::EndChild();
    ImGui::BeginChild("##stackB", ImVec2(0, 0), true, window_flags);

    if (ImGui::BeginTable("##stack", 4, flags))
    {
        ImGui::TableSetupColumn("Address (Hex)");
        ImGui::TableSetupColumn("Address (Dec)");
        ImGui::TableSetupColumn("Data (Hex)");
        ImGui::TableSetupColumn("Data (Dec)");
        ImGui::TableHeadersRow();
        int stkend = stkstart - 0x100 > 0 ? stkstart - 0x100 : 0;
        for (int row = stkstart; row >= stkend; row--)
        {
            ImGui::TableNextRow();
            ImGui::TableSetColumnIndex(0);
            ImGui::Text("%.4XH", row);
            ImGui::TableSetColumnIndex(1);
            ImGui::Text("%d",  row);
            ImGui::TableSetColumnIndex(2);
            ImGui::Text("%.2XH", UI.CPU->memory[row]);
            ImGui::TableSetColumnIndex(3);
            ImGui::Text("%d", UI.CPU->memory[row]);
        }
        ImGui::EndTable();
    }
    ImGui::EndChild();
    ImGui::End();
}

static void render_ports()
{
    ImGui::Begin("IO Ports");
    ImGuiWindowFlags window_flags = ImGuiWindowFlags_None;
    const static ImGuiTableFlags flags = ImGuiTableFlags_RowBg | ImGuiTableFlags_Resizable;
    ImGui::BeginChild("##ioT", ImVec2(0, 96), true, window_flags);

    ImGui::Text("Start (Hex)");
    ImGui::SameLine();
    static uint8_t iostart = 0;
    ImGui::InputScalar("##iohex", ImGuiDataType_U8, &iostart, NULL, NULL, "%x");
    ImGui::Text("Start (Dec)");
    ImGui::SameLine();
    ImGui::InputScalar("##iodec", ImGuiDataType_U8, &iostart, NULL, NULL, "%u");
    if (ImGui::Button("Reset"))
        memset(UI.CPU->ports, 0, 0x100);

    ImGui::EndChild();
    ImGui::BeginChild("##ioB", ImVec2(0, 0), true, window_flags);
    static int selected = -1;

    char buf[32];
    if (ImGui::BeginTable("##io", 4, flags)) {
        ImGui::TableSetupColumn("Address (Hex)");
        ImGui::TableSetupColumn("Address (Dec)");
        ImGui::TableSetupColumn("Data (Hex)");
        ImGui::TableSetupColumn("Data (Dec)");
        ImGui::TableHeadersRow();

        for (uint16_t row = iostart; row < 0x100; row++)
        {
            ImGui::TableNextRow();
            ImGui::TableSetColumnIndex(0);
            ImGui::Text("%.2XH", row);
            ImGui::TableSetColumnIndex(1);
            ImGui::Text("%d"  , row);
            ImGui::TableSetColumnIndex(2);
            if (selected == row)
            {
                if (ImGui::InputScalar("##ioinpH", ImGuiDataType_U8, &UI.CPU->ports[selected], NULL, NULL, "%.2X",
                        ImGuiInputTextFlags_AlwaysOverwrite | ImGuiInputTextFlags_EnterReturnsTrue))
                    selected = -1;
            }
            else
            {
                sprintf(buf, "##iorowh%d", row);
                if (ImGui::Selectable(buf, selected == row))
                    selected = row;
                ImGui::SameLine();
                ImGui::Text("%.2XH", UI.CPU->ports[row]);
            }
            ImGui::TableSetColumnIndex(3);
            if (selected == row)
            {
                if (ImGui::InputScalar("##ioinpD", ImGuiDataType_U8, &UI.CPU->ports[selected], NULL, NULL, "%d",
                        ImGuiInputTextFlags_AlwaysOverwrite | ImGuiInputTextFlags_EnterReturnsTrue))
                    selected = -1;
            }
            else
            {
                sprintf(buf, "##iorowd%d", row);
                if (ImGui::Selectable(buf, selected == row))
                    selected = row;
                ImGui::SameLine();
                ImGui::Text("%d", UI.CPU->ports[row]);
            }
        }
        ImGui::EndTable();
    }
    ImGui::EndChild();
    ImGui::End();
}

static void render_memory()
{
    ImGui::Begin("Memory");
    ImGuiWindowFlags window_flags = ImGuiWindowFlags_None;
    const static ImGuiTableFlags flags = ImGuiTableFlags_RowBg | ImGuiTableFlags_Resizable;
    ImGui::BeginChild("##memoryT", ImVec2(0, 96), true, window_flags);

    static uint16_t memstart = 0;
    ImGui::Text("Start (Hex)");
    ImGui::SameLine();
    ImGui::InputScalar("##memhex", ImGuiDataType_U16, &memstart, NULL, NULL, "%x");
    ImGui::Text("Start (Dec)");
    ImGui::SameLine();
    ImGui::InputScalar("##memdec", ImGuiDataType_U16, &memstart, NULL, NULL, "%u");
    if (ImGui::Button("Reset"))
    {
        UI.state->assembled = false;
        UI.changed = true;
        memset(UI.CPU->memory, 0, 0x10000);
    }

    ImGui::EndChild();
    ImGui::BeginChild("##memoryB", ImVec2(0, 0), true, window_flags);

    int memend = memstart + 0x100 < 0x10000 ? memstart + 0x100 : 0x10000;
    char buf[32];
    if (ImGui::BeginTable("##memory", 4, flags))
    {
        ImGui::TableSetupColumn("Address (Hex)");
        ImGui::TableSetupColumn("Address (Dec)");
        ImGui::TableSetupColumn("Data (Hex)");
        ImGui::TableSetupColumn("Data (Dec)");
        ImGui::TableHeadersRow();
        static int selected = -1;
        for (int row = memstart; row < memend; row++)
        {
            ImGui::TableNextRow();
            ImGui::TableSetColumnIndex(0);
            ImGui::Text("%.4XH", row);
            ImGui::TableSetColumnIndex(1);
            ImGui::Text("%d", row);
            ImGui::TableSetColumnIndex(2);
            if (selected == row)
            {
                if (ImGui::InputScalar("##meminpH", ImGuiDataType_U8, &UI.CPU->memory[selected], NULL, NULL, "%.2X",
                        ImGuiInputTextFlags_AlwaysOverwrite | ImGuiInputTextFlags_EnterReturnsTrue))
                    selected = -1;
            }
            else
            {
                sprintf(buf, "##memrowh%d", row);
                if (ImGui::Selectable(buf, selected == row))
                    selected = row;
                ImGui::SameLine();
                ImGui::Text("%.2XH", UI.CPU->memory[row]);
            }
            ImGui::TableSetColumnIndex(3);
            if (selected == row)
            {
                if (ImGui::InputScalar("##meminpD", ImGuiDataType_U8, &UI.CPU->memory[selected], NULL, NULL, "%d",
                        ImGuiInputTextFlags_AlwaysOverwrite | ImGuiInputTextFlags_EnterReturnsTrue))
                    selected = -1;
            }
            else
            {
                sprintf(buf, "##memrowd%d", row);
                if (ImGui::Selectable(buf, selected == row))
                    selected = row;
                ImGui::SameLine();
                ImGui::Text("%d", UI.CPU->memory[row]);
            }
        }
        ImGui::EndTable();
    }
    ImGui::EndChild();
    ImGui::End();
}

void render_assembler()
{
    ImGui::Begin("Assembler");
    if (!UI.state->assembled)
    {
        auto err = assembler_error();
        UI.err_line = err.first;
        if (UI.err_line != -1)
            ImGui::Text("%d %s", err.first, err.second.c_str());
    }
    ImGui::End();
}
