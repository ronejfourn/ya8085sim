#include <iostream>
#include <sstream>
#include <unordered_map>
#include <vector>
#include <cstring>
#include "assembler.h"
#include "instructions.h"

struct opdata_t
{
    uint16_t address;
    uint32_t type;
    char iname[16];
    size_t ilen;
};

struct opcode_t
{
    int opcode = -1;
    opcode_t(int a) : opcode(a){}
    opcode_t() : opcode(-1){}
};

struct lexer_t
{
    const char *start;
    const char *last;
    const char *cur;
};

struct resolve_t
{
    int index;
    std::string_view label;
};

struct label_info_t
{
    int line = -1;
    int address = -1;
};

struct operand_t
{
    uint32_t type;
    uint32_t number;
    std::string_view contents;
};

struct opinfo_t
{
    int count = -1;
    uint32_t f = 0;
    uint32_t s = 0;
};

static struct
{
    const char *start;
    const char *last;
    const char *cur;
    appstate_t *state;
    cpu_t *CPU;
    std::unordered_map<std::string_view, opinfo_t> opinfo_map;
    std::unordered_map<std::string_view, opcode_t> opcode_map;
    int line_num;
    bool err = false;
    std::ostringstream err_str;
}
ASSEMBLER;

enum
{
    OP_REG_A  = 1 << 0,
    OP_REG_B  = 1 << 1,
    OP_REG_C  = 1 << 2,
    OP_REG_D  = 1 << 3,
    OP_REG_E  = 1 << 4,
    OP_REG_H  = 1 << 5,
    OP_REG_L  = 1 << 6,
    OP_REG_M  = 1 << 7,
    OP_RP_SP  = 1 << 8,
    OP_RP_PSW = 1 << 9,
    OP_BYTE   = 1 << 10,
    OP_DBYTE  = 1 << 11,
    OP_LABEL  = 1 << 12,
    OP_RSTI   = 1 << 13,
    OP_ADDR = OP_DBYTE | OP_LABEL,
    OP_DATA = OP_BYTE  | OP_LABEL,
    OP_RP_ANY = OP_REG_B | OP_REG_D | OP_REG_H,
    OP_REG_ANY = OP_REG_A | OP_REG_B | OP_REG_C | OP_REG_D | OP_REG_E | OP_REG_H | OP_REG_L | OP_REG_M,
};

static inline bool is_space(char ch)
{ return (ch == ' ' || ch == '\t' || ch == '\v' || ch == '\f'); }
static inline bool is_alpha(char ch)
{ return (ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z'); }
static inline bool is_num(char ch)
{ return (ch >= '0' && ch <= '9'); }
static inline bool is_alnum(char ch)
{ return is_alpha(ch) || is_num(ch); }
static char to_lcase(char ch)
{ return (ch >= 'A' && ch <= 'Z') ? ch - 'A' + 'a' : ch; }

static int get_line(lexer_t *dlexer)
{
    if (ASSEMBLER.cur >= ASSEMBLER.last || !*ASSEMBLER.cur)
        return 0;
    while (ASSEMBLER.cur < ASSEMBLER.last && *ASSEMBLER.cur != '\r' && *ASSEMBLER.cur != '\n' && *ASSEMBLER.cur)
        ASSEMBLER.cur ++;
    size_t len = ASSEMBLER.cur - ASSEMBLER.start;
    dlexer->start = ASSEMBLER.start;
    dlexer->last  = ASSEMBLER.start + len;
    dlexer->cur   = ASSEMBLER.start;
    ASSEMBLER.cur += *ASSEMBLER.cur == '\r';
    ASSEMBLER.cur += *ASSEMBLER.cur == '\n';
    ASSEMBLER.start = ASSEMBLER.cur;
    return 1;
}

static operand_t lexer_get_operand(lexer_t *lexer, uint32_t expected)
{
    operand_t op = {0};
    if (lexer->start >= lexer->last)
    {
        ASSEMBLER.err = true;
        ASSEMBLER.err_str << "incomplete instruction";
        return op;
    }

    if (is_alpha(*lexer->cur))
    {
        while (lexer->cur < lexer->last && is_alnum(*lexer->cur))
            lexer->cur++;
        op.type |= OP_LABEL;
        size_t len = lexer->cur - lexer->start;
        if (len == 1)
        {
            switch (*lexer->start)
            {
                case 'A':
                case 'a': op.type |= OP_REG_A; break;
                case 'B':
                case 'b': op.type |= OP_REG_B; break;
                case 'C':
                case 'c': op.type |= OP_REG_C; break;
                case 'D':
                case 'd': op.type |= OP_REG_D; break;
                case 'E':
                case 'e': op.type |= OP_REG_E; break;
                case 'H':
                case 'h': op.type |= OP_REG_H; break;
                case 'L':
                case 'l': op.type |= OP_REG_L; break;
                case 'M':
                case 'm': op.type |= OP_REG_M; break;
                default: break;
            }
        }
        else if (len == 2)
        {
            char a = *(lexer->start + 0);
            char b = *(lexer->start + 1);
            if ((a == 's' || a == 'S') && (b == 'p' || b == 'P'))
                op.type |= OP_RP_SP;
        }
        else if (len == 3)
        {
            char a = *(lexer->start + 0);
            char b = *(lexer->start + 1);
            char c = *(lexer->start + 2);
            if ((a == 'p' || a == 'P') && (b == 's' || b == 'S') && (c == 'w' || c == 'W'))
                op.type |= OP_RP_PSW;
        }
    }
    else if (is_num(*lexer->cur))
    {
        uint32_t dec, hex;
        while (lexer->cur < lexer->last && is_alnum(*lexer->cur))
            lexer->cur++;
        size_t len = lexer->cur - lexer->start;
        if (len == 1 && *lexer->start >= '0' && *lexer->start <= '7')
            op.type |= OP_RSTI;
        op.type = OP_DBYTE | OP_BYTE;
        if (*(lexer->cur - 1) == 'h' || *(lexer->cur - 1) == 'H')
        {
            for (const char *t = lexer->start; t < lexer->cur - 1; t ++)
            {
                if (is_num(*t))
                    hex = (hex << 4) | (*t - '0');
                else if (*t >= 'a' && *t <= 'f')
                    hex = (hex << 4) | (*t - 'a' + 0xA);
                else if (*t >= 'A' && *t <= 'F')
                    hex = (hex << 4) | (*t - 'A' + 0xA);
                else
                {
                    ASSEMBLER.err = true;
                    ASSEMBLER.err_str << "bad character '" << *t << "'";
                    return op;
                }
            }
            op.number = hex;
        }
        else
        {
            for (const char *t = lexer->start; t < lexer-> cur; t ++)
            {
                if (is_num(*t))
                    dec = (dec * 10) + (*t - '0');
                else
                {
                    ASSEMBLER.err = true;
                    ASSEMBLER.err_str << "bad character '" << *t << "'";
                    return op;
                }
            }
            op.number = dec;
        }
    }
    else
    {
        ASSEMBLER.err = true;
        ASSEMBLER.err_str << "bad character '" << *lexer->start << "'";
        return op;
    }

    if (!(op.type & expected))
    {
        ASSEMBLER.err = true;
        ASSEMBLER.err_str << "invalid operand";
        return op;
    }

    size_t len  = lexer->cur - lexer->start;
    op.contents = {lexer->start, len};
    lexer->start = lexer->cur;
    return op;
}

static void lexer_strip_left(lexer_t *lexer)
{
    if (lexer->cur >= lexer->last)
        return;
    lexer->cur   = lexer->start;
    while (lexer->cur <= lexer->last  && is_space(*(lexer->cur++)));
    lexer->start = lexer->cur - 1;
    lexer->cur   = lexer->start;
}

static void lexer_strip_right(lexer_t *lexer)
{
    if (lexer->cur >= lexer->last)
        return;
    lexer->cur   = lexer->last;
    while (lexer->cur >= lexer->start && is_space(*(--lexer->cur)));
    lexer->last  = lexer->cur + 1;
    lexer->cur   = lexer->start;
}

static void lexer_strip_comment(lexer_t *lexer)
{
    if (lexer->cur >= lexer->last)
        return;
    while (lexer->cur < lexer->last && *(lexer->cur ++) != ';');
    lexer->last = lexer->cur - (lexer->cur != lexer->last);
    lexer->cur  = lexer->start;
}

static void load_operand(operand_t& op, opdata_t& odata, cpu_t& CPU, std::vector<resolve_t>&unresolved)
{
    if (op.type & odata.type & OP_LABEL)
    {
        resolve_t r;
        r.index = ASSEMBLER.line_num - 1;
        r.label = op.contents;
        unresolved.push_back(r);
    }
    else if (op.type & odata.type & OP_BYTE)
    {
        CPU.memory[odata.address + 1] = op.number & 0xff;
    }
    else if (op.type & odata.type & OP_DBYTE)
    {
        CPU.memory[odata.address + 1] = (op.number >> 0) & 0xff;
        CPU.memory[odata.address + 2] = (op.number >> 8) & 0xff;
    }
    else
    {
        size_t opl = op.contents.length();
        odata.iname[odata.ilen++] = ' ';
        for (size_t i = 0; i < opl; i++)
            odata.iname[odata.ilen++] = to_lcase(op.contents.at(i));
    }
}

std::pair<int, std::string> assembler_error()
{
    return {ASSEMBLER.err ? ASSEMBLER.line_num : -1, ASSEMBLER.err_str.str()};
}

bool assemble()
{
    lexer_t llexer;
    int& line_num = ASSEMBLER.line_num;

    ASSEMBLER.start = ASSEMBLER.state->code.data;
    ASSEMBLER.last = ASSEMBLER.state->code.data + ASSEMBLER.state->code.length;
    ASSEMBLER.cur = ASSEMBLER.state->code.data;
    ASSEMBLER.state->lines.clear();
    ASSEMBLER.line_num = 1;
    ASSEMBLER.err = false;
    ASSEMBLER.err_str.clear();

    uint16_t cur_addr = ASSEMBLER.state->loadat;
    std::unordered_map<std::string_view, label_info_t> label_map;
    std::vector<resolve_t>unresolved;
    opdata_t odata;

    while (get_line(&llexer))
    {
        line_t cur_line;
        size_t line_len  = llexer.last - llexer.start;
        cur_line.data    = {llexer.start, line_len};
        cur_line.address = -1;
        cur_line.nbytes  = 0;
        ASSEMBLER.state->lines.push_back(cur_line);
    }

    for (auto& line : ASSEMBLER.state->lines)
    {
        memset(&odata, 0, sizeof(odata));
        llexer.start = line.data.begin();
        llexer.last  = line.data.end();
        llexer.cur   = line.data.begin();
        lexer_strip_comment(&llexer);
        lexer_strip_left (&llexer);
        lexer_strip_right(&llexer);
        if (llexer.last == llexer.start)
        {
            line_num ++;
            continue;
        }
        if (!is_alpha(*llexer.start))
        {
            ASSEMBLER.err = true;
            ASSEMBLER.err_str << "unexpected character '" << *llexer.start << "'";
            break;
        }

        std::string_view word;
        bool has_label = false;
        size_t len = 0;

        while (llexer.cur < llexer.last)
        {
            if (is_space(*llexer.cur))
                break;
            if (*llexer.cur == ':')
            {
                llexer.cur ++;
                has_label = true;
                break;
            }
            len ++;
            llexer.cur ++;
        }

        word = {llexer.start, len};
        llexer.start = llexer.cur;

        if (has_label)
        {
            label_info_t info = {line_num, cur_addr};
            bool new_key = label_map.emplace(word, info).second;
            if (!new_key)
            {
                ASSEMBLER.err = true;
                ASSEMBLER.err_str << "duplicate label '" << word << "' previously defined on line " << label_map[word].line;
                break;
            }
            lexer_strip_left(&llexer);
            while (llexer.cur < llexer.last && !is_space(*(++llexer.cur)));
            len  = llexer.cur - llexer.start;
            word = {llexer.start, len};
        }

        llexer.start = llexer.cur;
        lexer_strip_left(&llexer);

        if (len > 4)
        {
            ASSEMBLER.err = true;
            ASSEMBLER.err_str << "invalid instruction";
            break;
        }

        for (size_t i = 0; i < len; i ++)
            odata.iname[i] = to_lcase(word.at(i));
        odata.ilen    = len;
        odata.address = cur_addr;
        std::string_view instruction = {odata.iname, odata.ilen};
        opinfo_t &info = ASSEMBLER.opinfo_map[instruction];

        if (info.count == -1)
        {
            ASSEMBLER.err = true;
            ASSEMBLER.err_str << "invalid instruction";
            break;
        }

        if (info.count == 0)
        {
            if (llexer.start != llexer.last)
            {
                ASSEMBLER.err = true;
                ASSEMBLER.err_str << "expected end of line";
                break;
            }
        }
        else if (info.count == 1)
        {
            operand_t op = lexer_get_operand(&llexer, info.f);
            if (ASSEMBLER.err) break;

            odata.type = info.f;
            if (instruction == "rst")
            {
                odata.iname[len++] = ' ';
                odata.iname[len++] = op.contents.at(0);
            }
            else load_operand(op, odata, *ASSEMBLER.CPU, unresolved);

            lexer_strip_left(&llexer);
            if (llexer.start != llexer.last)
            {
                ASSEMBLER.err = true;
                ASSEMBLER.err_str << "expected end of line";
                break;
            }
        }
        else if (info.count == 2)
        {
            operand_t op1 = lexer_get_operand(&llexer, info.f);
            if (ASSEMBLER.err) break;

            lexer_strip_left(&llexer);
            if (*llexer.start != ',')
            {
                ASSEMBLER.err = true;
                ASSEMBLER.err_str << "incomplete instruction";
                break;
            }
            llexer.start ++;
            llexer.cur = llexer.start;
            lexer_strip_left(&llexer);

            operand_t op2 = lexer_get_operand(&llexer, info.s);
            if (ASSEMBLER.err) break;

            lexer_strip_left(&llexer);
            if (llexer.start != llexer.last)
            {
                ASSEMBLER.err = true;
                ASSEMBLER.err_str << "expected end of line";
                break;
            }

            bool is_reg_m1 = (op1.type & OP_REG_M) != 0;
            bool is_reg_m2 = (op2.type & OP_REG_M) != 0;

            if ((instruction == "mov") && is_reg_m1 && is_reg_m2)
            {
                ASSEMBLER.err = true;
                ASSEMBLER.err_str << "invalid instruction";
                break;
            }

            odata.type = info.f;
            load_operand(op1, odata, *ASSEMBLER.CPU, unresolved);
            odata.iname[odata.ilen++] = ',';
            odata.type = info.s;
            load_operand(op2, odata, *ASSEMBLER.CPU, unresolved);
        }

        instruction = {odata.iname, odata.ilen};
        int opcode = ASSEMBLER.opcode_map[instruction].opcode;
        ASSEMBLER.CPU->memory[cur_addr] = opcode;
        line.address = cur_addr;

        if (info.s == OP_ADDR || info.f == OP_ADDR)
            line.nbytes = 3;
        else if (info.s == OP_DATA || info.f == OP_DATA)
            line.nbytes = 2;
        else
            line.nbytes = 1;

        cur_addr += line.nbytes;
        line_num ++;
    }

    if (ASSEMBLER.err) return false;

    for (auto& ur : unresolved)
    {
        int address  = label_map[ur.label].address;
        line_t& line = ASSEMBLER.state->lines[ur.index];

        if (address == -1)
        {
            ASSEMBLER.err = true;
            ASSEMBLER.err_str << "undefined label '" << ur.label << "'" << std::endl;
            break;
        }

        if (line.nbytes == 2)
        {
            ASSEMBLER.CPU->memory[line.address + 1] = address & 0xff;
        }
        else if (line.nbytes == 3)
        {
            ASSEMBLER.CPU->memory[line.address + 1] = (address >> 0) & 0xff;
            ASSEMBLER.CPU->memory[line.address + 2] = (address >> 8) & 0xff;
        }
        else
        {
            ASSEMBLER.err = true;
            ASSEMBLER.err_str << "impossible";
            break;
        }
    }

    return !ASSEMBLER.err;
}

void assembler_init(cpu_t *CPU, appstate_t *state)
{
    ASSEMBLER.CPU = CPU;
    ASSEMBLER.state = state;
    ASSEMBLER.opinfo_map["mov" ] = {2, OP_REG_ANY, OP_REG_ANY};
    ASSEMBLER.opinfo_map["mvi" ] = {2, OP_REG_ANY, OP_DATA};
    ASSEMBLER.opinfo_map["lxi" ] = {2, OP_RP_ANY | OP_RP_SP, OP_ADDR};
    ASSEMBLER.opinfo_map["ldax"] = {1, OP_REG_B | OP_REG_D};
    ASSEMBLER.opinfo_map["stax"] = {1, OP_REG_B | OP_REG_D};
    ASSEMBLER.opinfo_map["lhld"] = {1, OP_ADDR};
    ASSEMBLER.opinfo_map["shld"] = {1, OP_ADDR};
    ASSEMBLER.opinfo_map["lda" ] = {1, OP_ADDR};
    ASSEMBLER.opinfo_map["sta" ] = {1, OP_ADDR};
    ASSEMBLER.opinfo_map["xchg"] = {0};
    ASSEMBLER.opinfo_map["add" ] = {1, OP_REG_ANY};
    ASSEMBLER.opinfo_map["adc" ] = {1, OP_REG_ANY};
    ASSEMBLER.opinfo_map["sub" ] = {1, OP_REG_ANY};
    ASSEMBLER.opinfo_map["sbb" ] = {1, OP_REG_ANY};
    ASSEMBLER.opinfo_map["inr" ] = {1, OP_REG_ANY};
    ASSEMBLER.opinfo_map["dcr" ] = {1, OP_REG_ANY};
    ASSEMBLER.opinfo_map["ana" ] = {1, OP_REG_ANY};
    ASSEMBLER.opinfo_map["xra" ] = {1, OP_REG_ANY};
    ASSEMBLER.opinfo_map["ora" ] = {1, OP_REG_ANY};
    ASSEMBLER.opinfo_map["cmp" ] = {1, OP_REG_ANY};
    ASSEMBLER.opinfo_map["inx" ] = {1, OP_RP_ANY | OP_RP_SP};
    ASSEMBLER.opinfo_map["dcx" ] = {1, OP_RP_ANY | OP_RP_SP};
    ASSEMBLER.opinfo_map["dad" ] = {1, OP_RP_ANY | OP_RP_SP};
    ASSEMBLER.opinfo_map["adi" ] = {1, OP_DATA};
    ASSEMBLER.opinfo_map["aci" ] = {1, OP_DATA};
    ASSEMBLER.opinfo_map["sui" ] = {1, OP_DATA};
    ASSEMBLER.opinfo_map["sbi" ] = {1, OP_DATA};
    ASSEMBLER.opinfo_map["ani" ] = {1, OP_DATA};
    ASSEMBLER.opinfo_map["xri" ] = {1, OP_DATA};
    ASSEMBLER.opinfo_map["ori" ] = {1, OP_DATA};
    ASSEMBLER.opinfo_map["cpi" ] = {1, OP_DATA};
    ASSEMBLER.opinfo_map["daa" ] = {0};
    ASSEMBLER.opinfo_map["cma" ] = {0};
    ASSEMBLER.opinfo_map["stc" ] = {0};
    ASSEMBLER.opinfo_map["cmc" ] = {0};
    ASSEMBLER.opinfo_map["rlc" ] = {0};
    ASSEMBLER.opinfo_map["rrc" ] = {0};
    ASSEMBLER.opinfo_map["ral" ] = {0};
    ASSEMBLER.opinfo_map["rar" ] = {0};
    ASSEMBLER.opinfo_map["jmp" ] = {1, OP_ADDR};
    ASSEMBLER.opinfo_map["jnz" ] = {1, OP_ADDR};
    ASSEMBLER.opinfo_map["jz"  ] = {1, OP_ADDR};
    ASSEMBLER.opinfo_map["jnc" ] = {1, OP_ADDR};
    ASSEMBLER.opinfo_map["jc"  ] = {1, OP_ADDR};
    ASSEMBLER.opinfo_map["jpo" ] = {1, OP_ADDR};
    ASSEMBLER.opinfo_map["jpe" ] = {1, OP_ADDR};
    ASSEMBLER.opinfo_map["jp"  ] = {1, OP_ADDR};
    ASSEMBLER.opinfo_map["jm"  ] = {1, OP_ADDR};
    ASSEMBLER.opinfo_map["call"] = {1, OP_ADDR};
    ASSEMBLER.opinfo_map["cnz" ] = {1, OP_ADDR};
    ASSEMBLER.opinfo_map["cz"  ] = {1, OP_ADDR};
    ASSEMBLER.opinfo_map["cnc" ] = {1, OP_ADDR};
    ASSEMBLER.opinfo_map["cc"  ] = {1, OP_ADDR};
    ASSEMBLER.opinfo_map["cpo" ] = {1, OP_ADDR};
    ASSEMBLER.opinfo_map["cpe" ] = {1, OP_ADDR};
    ASSEMBLER.opinfo_map["cp"  ] = {1, OP_ADDR};
    ASSEMBLER.opinfo_map["cm"  ] = {1, OP_ADDR};
    ASSEMBLER.opinfo_map["ret" ] = {0};
    ASSEMBLER.opinfo_map["rnz" ] = {0};
    ASSEMBLER.opinfo_map["rz"  ] = {0};
    ASSEMBLER.opinfo_map["rnc" ] = {0};
    ASSEMBLER.opinfo_map["rc"  ] = {0};
    ASSEMBLER.opinfo_map["rpo" ] = {0};
    ASSEMBLER.opinfo_map["rpe" ] = {0};
    ASSEMBLER.opinfo_map["rp"  ] = {0};
    ASSEMBLER.opinfo_map["rm"  ] = {0};
    ASSEMBLER.opinfo_map["pchl"] = {0};
    ASSEMBLER.opinfo_map["push"] = {1, OP_RP_ANY | OP_RP_PSW};
    ASSEMBLER.opinfo_map["pop" ] = {1, OP_RP_ANY | OP_RP_PSW};
    ASSEMBLER.opinfo_map["xthl"] = {0};
    ASSEMBLER.opinfo_map["sphl"] = {0};
    ASSEMBLER.opinfo_map["in"  ] = {1, OP_DATA};
    ASSEMBLER.opinfo_map["out" ] = {1, OP_DATA};
    ASSEMBLER.opinfo_map["di"  ] = {0};
    ASSEMBLER.opinfo_map["ei"  ] = {0};
    ASSEMBLER.opinfo_map["nop" ] = {0};
    ASSEMBLER.opinfo_map["hlt" ] = {0};
    ASSEMBLER.opinfo_map["rst" ] = {1, OP_RSTI};

    ASSEMBLER.opcode_map["mov a, a"] = MOV_A_A;
    ASSEMBLER.opcode_map["mov a, b"] = MOV_A_B;
    ASSEMBLER.opcode_map["mov a, c"] = MOV_A_C;
    ASSEMBLER.opcode_map["mov a, d"] = MOV_A_D;
    ASSEMBLER.opcode_map["mov a, e"] = MOV_A_E;
    ASSEMBLER.opcode_map["mov a, h"] = MOV_A_H;
    ASSEMBLER.opcode_map["mov a, l"] = MOV_A_L;
    ASSEMBLER.opcode_map["mov a, m"] = MOV_A_M;
    ASSEMBLER.opcode_map["mov b, a"] = MOV_B_A;
    ASSEMBLER.opcode_map["mov b, b"] = MOV_B_B;
    ASSEMBLER.opcode_map["mov b, c"] = MOV_B_C;
    ASSEMBLER.opcode_map["mov b, d"] = MOV_B_D;
    ASSEMBLER.opcode_map["mov b, e"] = MOV_B_E;
    ASSEMBLER.opcode_map["mov b, h"] = MOV_B_H;
    ASSEMBLER.opcode_map["mov b, l"] = MOV_B_L;
    ASSEMBLER.opcode_map["mov b, m"] = MOV_B_M;
    ASSEMBLER.opcode_map["mov c, a"] = MOV_C_A;
    ASSEMBLER.opcode_map["mov c, b"] = MOV_C_B;
    ASSEMBLER.opcode_map["mov c, c"] = MOV_C_C;
    ASSEMBLER.opcode_map["mov c, d"] = MOV_C_D;
    ASSEMBLER.opcode_map["mov c, e"] = MOV_C_E;
    ASSEMBLER.opcode_map["mov c, h"] = MOV_C_H;
    ASSEMBLER.opcode_map["mov c, l"] = MOV_C_L;
    ASSEMBLER.opcode_map["mov c, m"] = MOV_C_M;
    ASSEMBLER.opcode_map["mov d, a"] = MOV_D_A;
    ASSEMBLER.opcode_map["mov d, b"] = MOV_D_B;
    ASSEMBLER.opcode_map["mov d, c"] = MOV_D_C;
    ASSEMBLER.opcode_map["mov d, d"] = MOV_D_D;
    ASSEMBLER.opcode_map["mov d, e"] = MOV_D_E;
    ASSEMBLER.opcode_map["mov d, h"] = MOV_D_H;
    ASSEMBLER.opcode_map["mov d, l"] = MOV_D_L;
    ASSEMBLER.opcode_map["mov d, m"] = MOV_D_M;
    ASSEMBLER.opcode_map["mov e, a"] = MOV_E_A;
    ASSEMBLER.opcode_map["mov e, b"] = MOV_E_B;
    ASSEMBLER.opcode_map["mov e, c"] = MOV_E_C;
    ASSEMBLER.opcode_map["mov e, d"] = MOV_E_D;
    ASSEMBLER.opcode_map["mov e, e"] = MOV_E_E;
    ASSEMBLER.opcode_map["mov e, h"] = MOV_E_H;
    ASSEMBLER.opcode_map["mov e, l"] = MOV_E_L;
    ASSEMBLER.opcode_map["mov e, m"] = MOV_E_M;
    ASSEMBLER.opcode_map["mov h, a"] = MOV_H_A;
    ASSEMBLER.opcode_map["mov h, b"] = MOV_H_B;
    ASSEMBLER.opcode_map["mov h, c"] = MOV_H_C;
    ASSEMBLER.opcode_map["mov h, d"] = MOV_H_D;
    ASSEMBLER.opcode_map["mov h, e"] = MOV_H_E;
    ASSEMBLER.opcode_map["mov h, h"] = MOV_H_H;
    ASSEMBLER.opcode_map["mov h, l"] = MOV_H_L;
    ASSEMBLER.opcode_map["mov h, m"] = MOV_H_M;
    ASSEMBLER.opcode_map["mov l, a"] = MOV_L_A;
    ASSEMBLER.opcode_map["mov l, b"] = MOV_L_B;
    ASSEMBLER.opcode_map["mov l, c"] = MOV_L_C;
    ASSEMBLER.opcode_map["mov l, d"] = MOV_L_D;
    ASSEMBLER.opcode_map["mov l, e"] = MOV_L_E;
    ASSEMBLER.opcode_map["mov l, h"] = MOV_L_H;
    ASSEMBLER.opcode_map["mov l, l"] = MOV_L_L;
    ASSEMBLER.opcode_map["mov l, m"] = MOV_L_M;
    ASSEMBLER.opcode_map["mov m, a"] = MOV_M_A;
    ASSEMBLER.opcode_map["mov m, b"] = MOV_M_B;
    ASSEMBLER.opcode_map["mov m, c"] = MOV_M_C;
    ASSEMBLER.opcode_map["mov m, d"] = MOV_M_D;
    ASSEMBLER.opcode_map["mov m, e"] = MOV_M_E;
    ASSEMBLER.opcode_map["mov m, h"] = MOV_M_H;
    ASSEMBLER.opcode_map["mov m, l"] = MOV_M_L;
    ASSEMBLER.opcode_map["mvi a,"] = MVI_A;
    ASSEMBLER.opcode_map["mvi b,"] = MVI_B;
    ASSEMBLER.opcode_map["mvi c,"] = MVI_C;
    ASSEMBLER.opcode_map["mvi d,"] = MVI_D;
    ASSEMBLER.opcode_map["mvi e,"] = MVI_E;
    ASSEMBLER.opcode_map["mvi h,"] = MVI_H;
    ASSEMBLER.opcode_map["mvi l,"] = MVI_L;
    ASSEMBLER.opcode_map["mvi m,"] = MVI_M;
    ASSEMBLER.opcode_map["xchg"  ] = XCHG ;
    ASSEMBLER.opcode_map["lxi b,"]  = LXI_B ;
    ASSEMBLER.opcode_map["lxi d,"]  = LXI_D ;
    ASSEMBLER.opcode_map["lxi h,"]  = LXI_H ;
    ASSEMBLER.opcode_map["lxi sp,"] = LXI_SP;
    ASSEMBLER.opcode_map["ldax b"] = LDAX_B;
    ASSEMBLER.opcode_map["ldax d"] = LDAX_D;
    ASSEMBLER.opcode_map["stax b"] = STAX_B;
    ASSEMBLER.opcode_map["stax d"] = STAX_D;
    ASSEMBLER.opcode_map["lhld"] = LHLD;
    ASSEMBLER.opcode_map["shld"] = SHLD;
    ASSEMBLER.opcode_map["sta"]  = STA ;
    ASSEMBLER.opcode_map["lda"]  = LDA ;
    ASSEMBLER.opcode_map["add a"] = ADD_A;
    ASSEMBLER.opcode_map["add b"] = ADD_B;
    ASSEMBLER.opcode_map["add c"] = ADD_C;
    ASSEMBLER.opcode_map["add d"] = ADD_D;
    ASSEMBLER.opcode_map["add e"] = ADD_E;
    ASSEMBLER.opcode_map["add h"] = ADD_H;
    ASSEMBLER.opcode_map["add l"] = ADD_L;
    ASSEMBLER.opcode_map["add m"] = ADD_M;
    ASSEMBLER.opcode_map["adc a"] = ADC_A;
    ASSEMBLER.opcode_map["adc b"] = ADC_B;
    ASSEMBLER.opcode_map["adc c"] = ADC_C;
    ASSEMBLER.opcode_map["adc d"] = ADC_D;
    ASSEMBLER.opcode_map["adc e"] = ADC_E;
    ASSEMBLER.opcode_map["adc h"] = ADC_H;
    ASSEMBLER.opcode_map["adc l"] = ADC_L;
    ASSEMBLER.opcode_map["adc m"] = ADC_M;
    ASSEMBLER.opcode_map["sub a"] = SUB_A;
    ASSEMBLER.opcode_map["sub b"] = SUB_B;
    ASSEMBLER.opcode_map["sub c"] = SUB_C;
    ASSEMBLER.opcode_map["sub d"] = SUB_D;
    ASSEMBLER.opcode_map["sub e"] = SUB_E;
    ASSEMBLER.opcode_map["sub h"] = SUB_H;
    ASSEMBLER.opcode_map["sub l"] = SUB_L;
    ASSEMBLER.opcode_map["sub m"] = SUB_M;
    ASSEMBLER.opcode_map["sbb a"] = SBB_A;
    ASSEMBLER.opcode_map["sbb b"] = SBB_B;
    ASSEMBLER.opcode_map["sbb c"] = SBB_C;
    ASSEMBLER.opcode_map["sbb d"] = SBB_D;
    ASSEMBLER.opcode_map["sbb e"] = SBB_E;
    ASSEMBLER.opcode_map["sbb h"] = SBB_H;
    ASSEMBLER.opcode_map["sbb l"] = SBB_L;
    ASSEMBLER.opcode_map["sbb m"] = SBB_M;
    ASSEMBLER.opcode_map["inr a"] = INR_A;
    ASSEMBLER.opcode_map["inr b"] = INR_B;
    ASSEMBLER.opcode_map["inr c"] = INR_C;
    ASSEMBLER.opcode_map["inr d"] = INR_D;
    ASSEMBLER.opcode_map["inr e"] = INR_E;
    ASSEMBLER.opcode_map["inr h"] = INR_H;
    ASSEMBLER.opcode_map["inr l"] = INR_L;
    ASSEMBLER.opcode_map["inr m"] = INR_M;
    ASSEMBLER.opcode_map["dcr a"] = DCR_A;
    ASSEMBLER.opcode_map["dcr b"] = DCR_B;
    ASSEMBLER.opcode_map["dcr c"] = DCR_C;
    ASSEMBLER.opcode_map["dcr d"] = DCR_D;
    ASSEMBLER.opcode_map["dcr e"] = DCR_E;
    ASSEMBLER.opcode_map["dcr h"] = DCR_H;
    ASSEMBLER.opcode_map["dcr l"] = DCR_L;
    ASSEMBLER.opcode_map["dcr m"] = DCR_M;
    ASSEMBLER.opcode_map["inx b"]  = INX_B ;
    ASSEMBLER.opcode_map["inx d"]  = INX_D ;
    ASSEMBLER.opcode_map["inx h"]  = INX_H ;
    ASSEMBLER.opcode_map["inx sp"] = INX_SP;
    ASSEMBLER.opcode_map["dcx b"]  = DCX_B ;
    ASSEMBLER.opcode_map["dcx d"]  = DCX_D ;
    ASSEMBLER.opcode_map["dcx h"]  = DCX_H ;
    ASSEMBLER.opcode_map["dcx sp"] = DCX_SP;
    ASSEMBLER.opcode_map["dad b"]  = DAD_B ;
    ASSEMBLER.opcode_map["dad d"]  = DAD_D ;
    ASSEMBLER.opcode_map["dad h"]  = DAD_H ;
    ASSEMBLER.opcode_map["dad sp"] = DAD_SP;
    ASSEMBLER.opcode_map["daa"] = DAA;
    ASSEMBLER.opcode_map["cma"] = CMA;
    ASSEMBLER.opcode_map["stc"] = STC;
    ASSEMBLER.opcode_map["cmc"] = CMC;
    ASSEMBLER.opcode_map["rlc"] = RLC;
    ASSEMBLER.opcode_map["rrc"] = RRC;
    ASSEMBLER.opcode_map["ral"] = RAL;
    ASSEMBLER.opcode_map["rar"] = RAR;
    ASSEMBLER.opcode_map["ana a"] = ANA_A;
    ASSEMBLER.opcode_map["ana b"] = ANA_B;
    ASSEMBLER.opcode_map["ana c"] = ANA_C;
    ASSEMBLER.opcode_map["ana d"] = ANA_D;
    ASSEMBLER.opcode_map["ana e"] = ANA_E;
    ASSEMBLER.opcode_map["ana h"] = ANA_H;
    ASSEMBLER.opcode_map["ana l"] = ANA_L;
    ASSEMBLER.opcode_map["ana m"] = ANA_M;
    ASSEMBLER.opcode_map["xra a"] = XRA_A;
    ASSEMBLER.opcode_map["xra b"] = XRA_B;
    ASSEMBLER.opcode_map["xra c"] = XRA_C;
    ASSEMBLER.opcode_map["xra d"] = XRA_D;
    ASSEMBLER.opcode_map["xra e"] = XRA_E;
    ASSEMBLER.opcode_map["xra h"] = XRA_H;
    ASSEMBLER.opcode_map["xra l"] = XRA_L;
    ASSEMBLER.opcode_map["xra m"] = XRA_M;
    ASSEMBLER.opcode_map["ora a"] = ORA_A;
    ASSEMBLER.opcode_map["ora b"] = ORA_B;
    ASSEMBLER.opcode_map["ora c"] = ORA_C;
    ASSEMBLER.opcode_map["ora d"] = ORA_D;
    ASSEMBLER.opcode_map["ora e"] = ORA_E;
    ASSEMBLER.opcode_map["ora h"] = ORA_H;
    ASSEMBLER.opcode_map["ora l"] = ORA_L;
    ASSEMBLER.opcode_map["ora m"] = ORA_M;
    ASSEMBLER.opcode_map["cmp a"] = CMP_A;
    ASSEMBLER.opcode_map["cmp b"] = CMP_B;
    ASSEMBLER.opcode_map["cmp c"] = CMP_C;
    ASSEMBLER.opcode_map["cmp d"] = CMP_D;
    ASSEMBLER.opcode_map["cmp e"] = CMP_E;
    ASSEMBLER.opcode_map["cmp h"] = CMP_H;
    ASSEMBLER.opcode_map["cmp l"] = CMP_L;
    ASSEMBLER.opcode_map["cmp m"] = CMP_M;
    ASSEMBLER.opcode_map["adi"] = ADI;
    ASSEMBLER.opcode_map["aci"] = ACI;
    ASSEMBLER.opcode_map["sui"] = SUI;
    ASSEMBLER.opcode_map["sbi"] = SBI;
    ASSEMBLER.opcode_map["ani"] = ANI;
    ASSEMBLER.opcode_map["xri"] = XRI;
    ASSEMBLER.opcode_map["ori"] = ORI;
    ASSEMBLER.opcode_map["cpi"] = CPI;
    ASSEMBLER.opcode_map["jmp"] = JMP;
    ASSEMBLER.opcode_map["jnz"] = JNZ;
    ASSEMBLER.opcode_map["jz" ] = JZ ;
    ASSEMBLER.opcode_map["jnc"] = JNC;
    ASSEMBLER.opcode_map["jc" ] = JC ;
    ASSEMBLER.opcode_map["jpo"] = JPO;
    ASSEMBLER.opcode_map["jpe"] = JPE;
    ASSEMBLER.opcode_map["jp" ] = JP ;
    ASSEMBLER.opcode_map["jm" ] = JM ;
    ASSEMBLER.opcode_map["call"] = CALL;
    ASSEMBLER.opcode_map["cnz" ] = CNZ ;
    ASSEMBLER.opcode_map["cz"  ] = CZ  ;
    ASSEMBLER.opcode_map["cnc" ] = CNC ;
    ASSEMBLER.opcode_map["cc"  ] = CC  ;
    ASSEMBLER.opcode_map["cpo" ] = CPO ;
    ASSEMBLER.opcode_map["cpe" ] = CPE ;
    ASSEMBLER.opcode_map["cp"  ] = CP  ;
    ASSEMBLER.opcode_map["cm"  ] = CM  ;
    ASSEMBLER.opcode_map["ret"] = RET;
    ASSEMBLER.opcode_map["rnz"] = RNZ;
    ASSEMBLER.opcode_map["rz" ] = RZ ;
    ASSEMBLER.opcode_map["rnc"] = RNC;
    ASSEMBLER.opcode_map["rc" ] = RC ;
    ASSEMBLER.opcode_map["rpo"] = RPO;
    ASSEMBLER.opcode_map["rpe"] = RPE;
    ASSEMBLER.opcode_map["rp" ] = RP ;
    ASSEMBLER.opcode_map["rm" ] = RM ;
    ASSEMBLER.opcode_map["pchl"] = PCHL;
    ASSEMBLER.opcode_map["push b"] = PUSH_B;
    ASSEMBLER.opcode_map["push d"] = PUSH_D;
    ASSEMBLER.opcode_map["push h"] = PUSH_H;
    ASSEMBLER.opcode_map["push psw"] = PUSH_PSW;
    ASSEMBLER.opcode_map["pop b"] = POP_B;
    ASSEMBLER.opcode_map["pop d"] = POP_D;
    ASSEMBLER.opcode_map["pop h"] = POP_H;
    ASSEMBLER.opcode_map["pop psw"] = POP_PSW;
    ASSEMBLER.opcode_map["xthl"] = XTHL;
    ASSEMBLER.opcode_map["sphl"] = SPHL;
    ASSEMBLER.opcode_map["in"] = IN;
    ASSEMBLER.opcode_map["out"] = OUT;
    ASSEMBLER.opcode_map["di"] = DI;
    ASSEMBLER.opcode_map["ei"] = EI;
    ASSEMBLER.opcode_map["nop"] = NOP;
    ASSEMBLER.opcode_map["hlt"] = HLT;
    ASSEMBLER.opcode_map["rst 0"] = RST_0;
    ASSEMBLER.opcode_map["rst 1"] = RST_1;
    ASSEMBLER.opcode_map["rst 2"] = RST_2;
    ASSEMBLER.opcode_map["rst 3"] = RST_3;
    ASSEMBLER.opcode_map["rst 4"] = RST_4;
    ASSEMBLER.opcode_map["rst 5"] = RST_5;
    ASSEMBLER.opcode_map["rst 6"] = RST_6;
    ASSEMBLER.opcode_map["rst 7"] = RST_7;
}
